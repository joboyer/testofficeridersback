
const { ApolloServer } = require("apollo-server-express")
const { typeDefs } = require("./Schemas/TypeDefs")
const { resolvers } = require("./Schemas/Resolvers")
const express = require("express");
const app = express();
const PORT = 4242;
const server = new ApolloServer({ typeDefs, resolvers })

server.applyMiddleware({ app });

app.listen({ port: PORT }, () => {
    console.log("Server running");
})
