let ProductData = require("../PRODUCTS_DATA.json");

const resolvers = {
    Query: {
        getAllProduct() {
            return ProductData
        },

        getProductById(parent, args) {
            let res = new Array()
            console.log(ProductData)
            for (var i = 0; i < ProductData.length; i++) {
                if (ProductData[i].productId == args.id) {
                    res.push(ProductData[i])
                }
            }
            return res;
        },

        getProductByCat(parent, args) {
            let res = new Array()
            for (var i = 0; i < ProductData.length; i++) {
                if (ProductData[i].productCategory == args.productCategory) {
                    res.push(ProductData[i])
                }
            }
            return res;
        },

        getAlsoLike(parent, args) {
            let res = new Array()
            for (var i = 0; i < ProductData.length; i++) {
                if ((ProductData[i].productCategory == args.productCategory && ProductData[i].productRating >= 4) && (ProductData[i].productId != args.id && Math.random() < 0.2)) {
                    res.push(ProductData[i])
                }
            }
            return res;
        }

    }
}

module.exports = { resolvers };