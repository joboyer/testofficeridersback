const { gql } = require("apollo-server-express");

const typeDefs = gql`
    type Product {
        productId: Int!
        productCategory: String!
        productName: String!
        productImage: String!
        productStock: Boolean!
        productPrice: Float!
        ProductTva: Float!
        productRating: Int!
        productEan: Int!
        productDescription: String!
    }

    #Queries
    type Query {
        getAllProduct: [Product!]!
        getProductById(id: Int!): [Product!]!
        getProductByCat(productCategory: String!): [Product!]!
        getAlsoLike(productCategory: String!, id: Int!): [Product!]!
    }
`

module.exports = { typeDefs };